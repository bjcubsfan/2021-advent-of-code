#!/usr/bin/env python

import datetime


def perform_insertions(current_polymer, insertion_rules):
    new_polymer = []
    for first, second in zip(current_polymer, current_polymer[1:]):
        pair = f"{first}{second}"
        inserted = insertion_rules[pair]
        new_polymer.append(first)
        new_polymer.append(inserted)
        # Insert second when it becomes first
    # catch the last one
    new_polymer.append(current_polymer[-1])
    return "".join(new_polymer)


def most_minus_least(current_polymer):
    letters = key = sorted(list(set(current_polymer)))
    occurs = list()
    for letter in letters:
        num = current_polymer.count(letter)
        occurs.append(num)
    return max(occurs) - min(occurs)


def part_1(input_data, steps):
    input_data = input_data.strip()
    input_data = input_data.split("\n")
    template = input_data[0].strip()
    insertion_rules = dict()
    for line in input_data[2:]:
        key, value = line.split(" -> ")
        insertion_rules[key] = value
        # insertion_rules[key] = f"{key[0]}{value}{key[1]}"
    current_polymer = template
    start = datetime.datetime.now()
    for step in range(steps):
        # print(current_polymer)
        print(
            f"At step {step} after {(datetime.datetime.now() - start).seconds} seconds"
        )
        current_polymer = perform_insertions(current_polymer, insertion_rules)
    return most_minus_least(current_polymer)


def main():
    with open("input") as input_file:
        input_data = input_file.read()
    print(part_1(input_data, 10))
    # TOO SLOW
    # print(part_1(input_data, 40))


if __name__ == "__main__":
    main()
